Source: ruby-erubis
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Bryan McLellan <btm@loftninjas.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-erubis.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-erubis
Homepage: https://rubygems.org/gems/erubis/
Rules-Requires-Root: no

Package: ruby-erubis
Architecture: all
Depends: ${ruby:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: erubis (<< 2.7.0-1~),
          erubis-doc (<< 2.7.0-1~),
          liberubis-ruby (<< 2.7.0-1~),
          liberubis-ruby1.8 (<< 2.7.0-1~),
          liberubis-ruby1.9.1 (<< 2.7.0-1~)
Breaks: erubis (<< 2.7.0-1~),
        erubis-doc (<< 2.7.0-1~),
        liberubis-ruby (<< 2.7.0-1~),
        liberubis-ruby1.8 (<< 2.7.0-1~),
        liberubis-ruby1.9.1 (<< 2.7.0-1~)
Provides: erubis,
          erubis-doc,
          liberubis-ruby,
          liberubis-ruby1.8,
          liberubis-ruby1.9.1
Description: fast and extensible eRuby implementation which supports multi-language
 Erubis is a very fast eRuby implementation that features:
  * Multi-language support (Ruby/PHP/C/Java/Scheme/Perl/Javascript)
  * Auto escaping support
  * Auto trimming spaces around '<% %>'
  * Embedded pattern changeable (default '<% %>')
  * Enable to handle Processing Instructions (PI) as embedded pattern
  * Context object available and easy to combine eRuby template with YAML file
  * Print statement available
  * Easy to extend and customize in subclass
  * Ruby on Rails support
